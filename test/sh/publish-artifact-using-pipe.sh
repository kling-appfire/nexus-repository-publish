#!/usr/bin/env sh

# This runs the pipe in docker. It returns an exit code of 1 on error which will cause the test to fail.
docker-compose run testclient
