#!/usr/bin/env bash

# Copyright (c) 2019-present Sonatype, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -ex

VERSION=$(semversioner current-version)

echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin

# get tags for releases or development branches
if [[ "${BITBUCKET_BRANCH}" == "master" ]]; then
    tag="${VERSION}"
elif [[ "${BITBUCKET_BRANCH}" =~ "INT-" ]]; then
    tag="${VERSION}-${BITBUCKET_BRANCH}"
else
    echo "ERROR: Branch name must match master or INT-*. Skipping push to docker."
    exit 0
fi

docker build -t ${DOCKERHUB_REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER} .

docker tag "${DOCKERHUB_REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER}" "${DOCKERHUB_REPOSITORY}:${tag}"
docker push "${DOCKERHUB_REPOSITORY}:${tag}"

# create :latest tag for builds on master
if [[ "${BITBUCKET_BRANCH}" == "master" ]]; then
  docker tag "${DOCKERHUB_REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER}" "${DOCKERHUB_REPOSITORY}:latest"
  docker push "${DOCKERHUB_REPOSITORY}:latest"
fi

# Update image attribute in pipe.yml
sed -i "s%^image.*$%image: ${DOCKERHUB_REPOSITORY}:${tag}%g" pipe.yml
