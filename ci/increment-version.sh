#!/usr/bin/env bash

# Copyright (c) 2019-present Sonatype, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [[ "${BITBUCKET_BRANCH}" == "master" ]]; then

  # Remember previous version for string substitution in README.md and pipe.yml
  previous_version=$(semversioner current-version)
  semversioner add-change --type patch --description "`git log -1 --pretty=%B`"
  semversioner release
  new_version=$(semversioner current-version)

  # Substitute previous version with new version in files
  sed -i "s/:$previous_version/:$new_version/g" README.md
fi
